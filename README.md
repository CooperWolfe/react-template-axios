# Usage
Remember to:
 - Change all instances of "app-name" with the app's name
 - Change favicon.ico

# Sentry
To use sentry:
 1. run `npm install --save @sentry/browser`
 2. Uncomment the commented lines in the log service
 3. Get the sentry token and replace "code" with the token in the log service.
 4. Uncomment the commented sentry code in the axios service
