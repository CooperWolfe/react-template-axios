import http from 'axios'
import logService from './log'

http.defaults.baseURL = `http://${process.env.REACT_APP_BACKEND_ADDR}:${process.env.REACT_APP_BACKEND_PORT}`

const _401Urls = []
_401Urls.forEach((url, i) => _401Urls[i] = `${http.defaults.baseURL}/${url}`)

// Authorization error
http.interceptors.response.use(null, err => {
  if (err.response && err.response.status === 401 && !_401Urls.includes(err.request.responseURL))
    localStorage.removeItem('token')
  return Promise.reject(err)
})
// Unexpected errors
http.interceptors.response.use(null, err => {
  const errorExpected = err.response && err.response.status >= 400 && err.response.status < 500

  if (!errorExpected) {
    logService.log(err)
    logService.alertError(err.message || 'An unexpected error occurred.')
    // logService.logSentry(err)
  }

  return Promise.reject(err)
})
// Authentication
http.interceptors.request.use(req => {
  const token = localStorage.getItem('token')
  if (!token) return req
  req.headers['Authorization'] = `Bearer ${token}`
  return req
})
